resource "aws_instance" "app_server" {
  ami           = "ami-0a8e758f5e873d1c1" # amd64 ubuntu Server 20.04 image
  instance_type = "t2.micro"
  count         = 1
  key_name      = aws_key_pair.deployer.key_name

  tags = {
    Name = "app_server"
  }

}

output "app_instance_ips" {
  value = aws_instance.app_server.*.public_ip
}
