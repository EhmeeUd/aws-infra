terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  backend "s3" {
    bucket = "sca-infra-demo"
    key    = "tf-statefile"
    region = "eu-west-1"
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDu0L2P/o7TdU2mziKIOadPPsC+q7HUSWREXuOTQzfSfsIc7uOBx6b/5lmnJbUukcPAFpyv5YKCw3swen8dTczFKw+7V5WLE7Gtw7NIuxkaQNMBo3DVWkZVvrpFl9rImm9Az5O7RBAwFWQzsapcW7h3+ttxyAE776fXu5dS+33XWUl9thQ2p7viyX5RTWYDeXOzBcpSBbecQRUGcCDmI0HB2fD0+zZaaybdS1PNSMF88Ga4wIdU/1t6MsrXSh4zEjWFe03Lf6OElnY6+IXXPvnVbbAxxuGVMEw6SCtfPpYh9PuRVwAsqI2C524GZLfqL6/T3vLqF9iglr/wh6jEf4geZh7pjIh380m5xVTva8PoPvAgUS5L+NRJLhXGzE2ZVh+mePW5/UYJT1Hs60XgZhJFp2AaBFVrGJjPyPto6KEKQ3oeMABYRdhf9oGkzCKqAEf6ImRAUGMTD6VzjxfoWaolPHf0A1HYa6jqzlv6IfEvH/RG8zjeH/UMgJBHgCqx9ysE/5Xdd1jQdzK9xs9CBJ2Wn9ZfIJ3ZlanT6RWVkywNaI78jr6cJlanxf34Oa3M6F87nNMbxO82JDNZ34gVEK6z2eqQqRsJL4FAaP7efBwaYlquOUJzwRXOayAzrO55Rrhv/eUfQuxY8a/N1qpK94BK4gMlcZuB1T2MXKZx3w8eEw=="
}


